resource "aws_vpc" "main" {
  cidr_block = var.vpc_var_cidr

  tags = {
    Name = "main"
  }
}

resource "aws_subnet" "main" {
  
  vpc_id = aws_vpc.main.id

  cidr_block = var.subnets_var_cidr
  availability_zone = var.awsazs_var

  tags = {
    Name = "main"
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "main"
  }
}

resource "aws_route_table" "r" {
  vpc_id = aws_vpc.main.id
  route {
    cidr_block = "10.0.1.0/24"
    gateway_id = aws_internet_gateway.gw.id
  }
  
  tags = {
    Name = "main"
  }
}
