variable "aws_var_region" {
  default  = "eu-west-1"
}


variable "vpc_var_cidr" {
    default = "10.0.0.0/16"
}

variable "subnets_var_cidr" {
    default = "10.0.1.0/24"
}


variable "awsazs_var" {
	
	default = "eu-west-1a"
}

variable "webservers_var_ami" {
  default = "ami-0383535ce92966dfe"
}

variable "instance_var_type" {
  default = "t2.nano"
}

variable "security_var_group_ingress_rules" {
    type = list(object({
      from_port   = number
      to_port     = number
      protocol    = string
      cidr_block  = string
      description = string
    }))
    default     = [
        {
          from_port   = 22
          to_port     = 22
          protocol    = "tcp"
          cidr_block  = ["0.0.0.0/0"]
          description = "SSH - Only admin"
        },
        {
          from_port   = 80
          to_port     = 80
          protocol    = "tcp"
          cidr_block  = ["0.0.0.0/0"]
          description = "Cli"
        },
    ]
}

variable "security_var_group_egress_rules" {
  type = 
}
