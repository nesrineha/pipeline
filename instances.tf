resource "aws_instance" "webservers" {
	count = length(var.subnets_var_cidr) 
	ami = "(var.webservers_var_ami)"
	instance_var_type = var.instance_var_type
	security_groups = ["${aws_security_group.main-ingress.id}"]
    subnet_id = aws_subnet.main.id
}
